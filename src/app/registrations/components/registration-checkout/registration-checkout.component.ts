import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CurrentRegistrationService } from '../../services/currentRegistrationService.service';
import { ProfileService } from '../../../profile/services/profile.service';
import { LoadingService } from '@universis/common';
import { ModalService } from '@universis/common';
import { ViewChild, TemplateRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LocalizedDatePipe } from '@universis/common';
import {error} from 'util';

@Component({
  selector: 'app-registration-checkout',
  templateUrl: './registration-checkout.component.html',
  styleUrls: ['./registration-checkout.component.scss']
})
export class RegistrationCheckoutComponent implements OnInit {
  @ViewChild('templateSuccess') successTemplate: TemplateRef<any>;
  @ViewChild('templateFail') failTemplate: TemplateRef<any>;
  @ViewChild('templatePSuccess') psuccessTemplate: TemplateRef<any>;
  @ViewChild('notAvailableTemplate') notAvailableTemplate: TemplateRef<any>;

  private currentRegistration: any;
  private department: any;
  private coursesToRegister: any;
  private effectiveStatus: any;
  private code: any;
  public failedCourses: any = [];
  private totalEcts = 0;
  private totalUnits = 0;
  public loading = true;
  private openRegistrationPeriod = false;
  private isRegistrationPeriod: boolean;
  private isEmptyRegistration = false;
  public currentRegistrationResult: any;
  public isCollapsed: boolean[];
  public coursesNotAvailable: any;
  public collapsed = false;

  // boolean to check if student has edited his registration so we can
  // show/hide the "Submit" button respectively
  public registrationEdited = false;

  private registrationPeriodStart: Date;
  private registrationPeriodEnd: Date;
  public start: string;
  public end: string;
  private modalRef: any;
  public clicked = false;

  constructor(private currentRegService: CurrentRegistrationService,
              private route: ActivatedRoute,
              private profileService: ProfileService,
              private loadingService: LoadingService,
              private _translateService: TranslateService,
              private modal: ModalService) {
  }

  async ngOnInit() {
    try {
      this.loadingService.showLoading();  // show loading
      this.coursesNotAvailable = [];
      // get student department registration period details
      const student = await this.profileService.getStudent();
      this.department = student.department;
      this.registrationPeriodStart = this.department.registrationPeriodStart;
      this.registrationPeriodEnd = this.department.registrationPeriodEnd;

      this.start = new LocalizedDatePipe(this._translateService).transform(this.registrationPeriodStart, 'shortDate');
      this.end = new LocalizedDatePipe(this._translateService).transform(this.registrationPeriodEnd, 'shortDate');

      // get registration effective status
      this.effectiveStatus = await this.currentRegService.getCurrentRegistrationEffectiveStatus();
      if (this.effectiveStatus == null) {
        throw error;
      }
      this.code = this.effectiveStatus.code;
      if (this.effectiveStatus.status === 'open') {
        this.openRegistrationPeriod = true;
      }
      // get current registration
      this.currentRegistration = await this.currentRegService.getCurrentRegistration();

      // Perform a check to see if student changed the registration.
      // This is done by comparing the 2 arrays, so we need to encode the first
      // array into JSON(the second one already is encoded from session storage)
      // in order to be able to compare them.
      if (JSON.stringify(this.currentRegistration.classes) === sessionStorage['InitialRegistration']) {
        this.registrationEdited = false;
      } else {
        this.registrationEdited = true;
      }

      // Handle specialty skipping
      if (this.effectiveStatus.code === 'SELECT_SPECIALTY') {
        if (this.currentRegistration.status && this.currentRegistration.status.alternateName) {
          if (this.currentRegistration.status.alternateName === 'open') {
            this.effectiveStatus.code = 'P_SYSTEM_TRANSACTION';
          } else if (this.currentRegistration.status.alternateName === 'closed') {
            this.effectiveStatus.code = 'CLOSED_REGISTRATION';
          } else if (this.currentRegistration.status.alternateName === 'pending') {
            this.effectiveStatus.code = 'PENDING_TRANSACTION';
          }
        } else {
          this.effectiveStatus.code = 'OPEN_NO_TRANSACTION';
        }
        sessionStorage['registrationEffectiveStatus'] = JSON.stringify(this.effectiveStatus);
      }
      this.code = this.effectiveStatus.code;

      // If student made changes in the registration, update the effective status
      // to equal the fake status "CLIENT_EDIT_REGISTRATION" in order to show the appropriate
      // edit messages at checkout and not the old effective status messages.
      if (this.registrationEdited && this.effectiveStatus.code !== 'OPEN_NO_TRANSACTION') {
        this.code = 'CLIENT_EDIT_REGISTRATION';
      }

      this.coursesToRegister = this.currentRegistration.classes;

      if (this.effectiveStatus.status === 'open') {
        const availableClasses = await this.currentRegService.getAvailableClasses();
        // If there are courses in the registration
        if (this.currentRegistration.classes.length > 0) {
          this.currentRegistration.classes.forEach(x => {
            const availableClass = availableClasses.find(y => {
              if (x.courseClass) {
                return y.courseClass.id === x.courseClass.id;
              }
            });
            if (availableClass) {
              Object.assign(x, {
                name: availableClass.name,
                courseClass: availableClass.courseClass,
                displayCode: availableClass.displayCode,
                courseType: availableClass.courseType,
                specialty: availableClass.specialty,
                notAvailable: 0
              });
            } else {
              Object.assign(x, {
                name: x.courseClass.title,
                displayCode: x.courseClass.course.displayCode,
                notAvailable: 1
              });
            }
          });
        } else {
          // if there are no course as in the registration this boolean will
          // show a relevant message to the user
          this.isEmptyRegistration = true;
          this.coursesToRegister = [];
        }
        this.coursesNotAvailable =  this.coursesToRegister.filter( x => {
          return  x.notAvailable === 1; }
        );

        // calculate totalUnits and totalECTS
        this.calculateTotal();

        // Check whether we are between registration period deadlines
        this.isRegistrationPeriod = this.checkRegistrationPeriodDeadlines();
      } else {
        this.coursesToRegister.map(x => {
          Object.assign(x, {
            name: x.courseClass.title,
            displayCode: x.courseClass.course.displayCode
          });
        });
      }
      this.coursesToRegister.forEach( course => {
        if (course.semester && course.semester.id) {
          course.courseSemester = course.semester.id;
        } else {
          course.courseSemester = parseInt(course.semester, 10) || 0;
        }
      });
    } catch (error) {

      // TODO: Warn about the error.

      this.isEmptyRegistration = true;
      this.coursesToRegister = [];
      // Check whether we are between registration period deadlines
      this.isRegistrationPeriod = this.checkRegistrationPeriodDeadlines();
    }
    // hide loading
    this.loading = false;
    this.loadingService.hideLoading();
  }

  registerSelected() {
    this.loadingService.showLoading();  // show loading
    this.currentRegService.saveCurrentRegistration()
      .then(currentRegistrationResult => {

        if (!currentRegistrationResult) {
          throw new Error('Registration result is not specified');
        } else if (!currentRegistrationResult.validationResult) {
          throw new Error('Validation result is not specified');
        }

        if (currentRegistrationResult.validationResult.code === 'SUCC') {
          this.openModal(this.successTemplate);
        } else if (
          currentRegistrationResult.validationResult.code === 'FAIL' || currentRegistrationResult.validationResult.code === 'EFAIL'
        ) {
          this.openModal(this.failTemplate);
        } else {
          this.openModal(this.psuccessTemplate);
        }

        // If registration save was successful, purge all session storage data
        if (currentRegistrationResult.validationResult) {
          this.currentRegService.reset();
        }

        this.currentRegistrationResult = currentRegistrationResult;
        const registeredCourses = currentRegistrationResult.classes;

        registeredCourses.forEach((course: any) => {
          course.validationResult = course.validationResult || {
            success: false,
            code: 'FAIL',
            message: this._translateService.instant('Modals.unspecifiedError')
          };

          if (!course.validationResult.success) {
            this.failedCourses.push(course);
            this.isCollapsed = Array(this.failedCourses.length).fill(false);
          }
        });
        this.loadingService.hideLoading();
      }).catch((err) => {
        // TODO: Warn about the error.
        this.loadingService.hideLoading();

        // add general error to validationResult
        this.currentRegistrationResult = {
          validationResult : {
            code: 'FAIL',
            success: false,
            message: this._translateService.instant('Modals.unspecifiedError')
          }
        };
        this.currentRegService.reset();
        this.openModal(this.failTemplate);
      });
  }

  checkRegistrationPeriodDeadlines(): boolean {
    const today = new Date();
    if (this.registrationPeriodStart < today && this.registrationPeriodEnd > today) {return true; }
    return false;
  }

  // function that sums the total units and ects of the courses to be registered
  calculateTotal() {
    if (this.coursesToRegister) {
      this.totalUnits = 0;
      this.totalEcts = 0;
      for (let i = 0 ; i < this.coursesToRegister.length; ++i) {
        this.totalUnits += this.coursesToRegister[i].units;
        this.totalEcts += this.coursesToRegister[i].ects;
      }
    }
  }

  enableEdit() {
    sessionStorage.setItem('edit', 'TRUE');
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modal.openModal(template, 'modal-lg');
  }

  closeModal() {
    // Reload the page. A more specific function may be implemented later
    this.ngOnInit();
    this.modalRef.hide();
  }

  backToEdit() {
    this.enableEdit();
    this.closeModal();
  }

  removeCourses(courses) {
    this.currentRegService.getCurrentRegistration().then(currentReg => {
      // find course index
      courses.forEach((course) => {
        for (let i = 0; i < currentReg.classes.length; ++i) {
          if (this.currentRegService.identifierCompare(currentReg.classes[i].courseClass, course.courseClass)) {
            // remove course from current registration
            currentReg.classes.splice(i, 1);
            break;
          }
        }
      });
      // save to session storage
      sessionStorage.setItem('RegistrationLatest', JSON.stringify(currentReg));
      this.registerSelected();
    });
  }

}
