import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RegistrationsHomeComponent} from './components/registrations-home/registrations-home.component';
import {RegistrationListComponent} from './components/registrations-list/registrations-list.component';
import {RegistrationSemesterComponent} from './components/registrations-semester/registrations-semester.component';
import {RegistrationCoursesComponent} from './components/registrations-courses/registrations-courses.component';
import {CurrentRegistrationService} from './services/currentRegistrationService.service';
import {FormsModule} from '@angular/forms';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {HttpClientModule} from '@angular/common/http';
import {MostModule} from '@themost/angular';
import {RegistrationsRoutingModule} from './registrations-routing.routing';
import {environment} from '../../environments/environment';
import {SharedModule} from '@universis/common';
import {ProfileSharedModule} from '../profile/profile-shared.module';
import {RegistrationCheckoutComponent} from './components/registration-checkout/registration-checkout.component';
import {NgPipesModule} from 'ngx-pipes';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {RegistrationSpecialtyComponent} from './components/registration-specialty/registration-specialty.component';
import {ModalModule} from 'ngx-bootstrap/modal';
import {StudentsSharedModule} from '../students-shared/students-shared.module';
import { RegistrationsCourseClassComponent } from './components/registrations-course-class/registrations-course-class.component';
import {RegistrationSharedModule} from './registration-shared.module';
import {RegistrationCheckoutCanDeactivateGuard} from './services/registration-checkout-can-deactivate-guard';
import {RegistrationLeaveComponent} from './components/registration-leave-component/registration-leave.component';


@NgModule({
    imports: [
        HttpClientModule,
        CommonModule,
        FormsModule,
        TranslateModule,
        SharedModule,
        MostModule,
        RegistrationsRoutingModule,
        ProfileSharedModule,
        NgPipesModule,
        TooltipModule,
        ModalModule.forRoot(),
        StudentsSharedModule,
        RegistrationSharedModule
    ],
    entryComponents: [
        RegistrationSpecialtyComponent,
        RegistrationsCourseClassComponent,
        RegistrationLeaveComponent
    ],
  declarations: [
    RegistrationsHomeComponent,
    RegistrationListComponent,
    RegistrationSemesterComponent,
    RegistrationCoursesComponent,
    RegistrationCheckoutComponent,
    RegistrationSpecialtyComponent,
    RegistrationsCourseClassComponent,
    RegistrationLeaveComponent

  ],
    exports: [
        RegistrationSpecialtyComponent,
        RegistrationsCourseClassComponent,
        RegistrationLeaveComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    providers: [
      RegistrationCheckoutCanDeactivateGuard
    ]
})
export class RegistrationsModule {
    constructor(private _translateService: TranslateService) {
        environment.languages.forEach((culture) => {
            import(`./i18n/registrations.${culture}.json`).then((translations) => {
                this._translateService.setTranslation(culture, translations, true);
            });
        });
    }
}
