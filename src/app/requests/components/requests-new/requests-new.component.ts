import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfigurationService } from '@universis/common';
import { ApplicationSettings } from '../../../students-shared/students-shared.module';
import { ModalService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-requests-new',
  templateUrl: './requests-new.component.html',
  styleUrls: ['./requests-new.component.scss']
})
export class RequestsNewComponent implements OnInit {
  @ViewChild('templateFail') failTemplate: TemplateRef<any>;

  public requestHeader;
  public isLoading = false;
  public hasError = false;
  private modalRef: any;

  constructor(
    private route: ActivatedRoute,
    private _router: Router,
    private _context: AngularDataContext,
    private modalService: ModalService,
    private _translateService: TranslateService,
    private _configurationService: ConfigurationService) {
    this.model.alternateName = this.route.snapshot.queryParamMap.get('type');
    this.model.object.alternateName = this.route.snapshot.queryParamMap.get('type');
  }

  public model = {
    name: null,
    alternateName: null,
    description: null,
    object: {
      alternateName: null
    }
  };

  ngOnInit() {
    // get header request message from app.json
    const messages = this._configurationService.settings.app && (<ApplicationSettings>this._configurationService.settings.app).messages;
    if (Array.isArray(messages)) {
      const message = messages.find(x => {
        return x.additionalType === 'StudentRequestHeader' && x.inLanguage === this._configurationService.currentLocale;
      });
      if (message) {
        this.requestHeader = message.body;
      }
    }
  }

  async submit() {
    this.isLoading = true;
    this.hasError = false;

    const action = (this.model.alternateName === 'OtherRequest') ? 'RequestMessageActions' : 'RequestDocumentActions';
    if (action === 'RequestMessageActions') {
      this.model.object = null;
    }

    try {
      await this._context.model(action).save(this.model);
      return this._router.navigate(['/requests/list']);
    } catch (err) {
      this.hasError = true;
      this.isLoading = false;
      this.modalRef = this.modalService.openModal(this.failTemplate, 'modal-lg');
    }
  }

  closeModal(): void {
    this.modalRef.hide();
  }
}
