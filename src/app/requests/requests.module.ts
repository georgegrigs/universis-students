import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {RequestsRoutingModule} from './requests-routing.module';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {RequestsListComponent} from './components/requests-list/requests-list.component';
import {RequestsHomeComponent} from './components/requests-home/requests-home.component';
import {RequestsNewComponent} from './components/requests-new/requests-new.component';
import {ModalModule} from 'ngx-bootstrap';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '@universis/common';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        SharedModule,
        InfiniteScrollModule,
        RequestsRoutingModule,
        ModalModule
    ],
    declarations: [RequestsListComponent, RequestsHomeComponent, RequestsNewComponent]
})
export class RequestsModule {
    constructor(private _translateService: TranslateService) {
        environment.languages.forEach((culture) => {
            import(`./i18n/requests.${culture}.json`).then((translations) => {
                this._translateService.setTranslation(culture, translations, true);
            });
        });
    }
}
