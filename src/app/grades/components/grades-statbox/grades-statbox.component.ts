import { Component, OnInit, Input } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {GradesService} from '../../services/grades.service';
// import {ChartsModule} from 'ng2-charts';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-grades-statbox',
  templateUrl: './grades-statbox.component.html',
  styleUrls: ['./grades-statbox.component.scss']
})
export class GradesStatboxComponent implements OnInit {

  @Input() title: string;
  @Input() statsPeriod: string;
  @Input() labelTotal: string;
  @Input() labelPassed: string;
  @Input() registeredCourses: number;
  @Input() passedCourses: number;
  @Input() passedGradeWeightedAverage: string;
  @Input() passedGradeSimpleAverage: string;
  @Input() totalEcts: number;
  public doughnutChartData: number[];
  public doughnutChartType = 'doughnut';
  public doughnutChartLabels: string[] = [];
  public failedCourses: number;
  public doughnutColors = [
    {
        backgroundColor: [
          '#2500dd',
          '#BDB2F5',
        ]
      }
    ];
  public doughnutChartOptions = {};
  constructor(private _contextService: AngularDataContext,
              private _gradeService: GradesService,
              private _translateService: TranslateService) {
  }

  ngOnInit() {
    this.labelTotal = this.labelTotal ? this.labelTotal : this._translateService.instant('Grades.TotalCourses');
    this.labelPassed = this.labelPassed ? this.labelPassed : this._translateService.instant('Grades.PassedCourses');
    // this calculation should be correct as getGradesWeightedAverage filters complex courses which count as one.
    // Remove this comment before committing to your MR.
    this.failedCourses = this.registeredCourses - this.passedCourses;
    this.doughnutChartData = [this.passedCourses, this.failedCourses];

    const text = this.passedCourses + '/' + this.registeredCourses;
    this.doughnutChartOptions = Object.assign({
      cutoutPercentage: 80,
      legend: {
        display: false
      },
      centerText: {
        display: true,
        text: text
      },
      tooltips: {
        position: 'nearest'
      }
    });
    this._translateService.get('Grades.PassedCourses').subscribe(x => {
      this.doughnutChartLabels.push(x);
    });
    this._translateService.get('Grades.FailedCourses').subscribe( x => {
      this.doughnutChartLabels.push(x);
    });

  }

}
