import {MostModule} from '@themost/angular';
import {GradeScaleService} from '@universis/common';
import { ApiTestingModule, ApiTestingController } from '@universis/common/testing';
import {async, inject, TestBed} from '@angular/core/testing';
import {GradeAverageResult, GradesService} from './grades.service';
import {HttpClientTestingModule, TestRequest} from '@angular/common/http/testing';
import {grades, courseGrades, courseGrades1} from './calculate-grades-average-data';

// TODO: The following describe should be broken in 9 different describes to test more cases for each function
// Here a very basic test will be written for each function. We should really create mock data and
// test on those data by flushing them to the ApiTestingController instead of writing them in each test.

describe('GradeService', () => {
  let mockApi: ApiTestingController;
  const gradeScaleSvc = jasmine.createSpyObj('GradeScaleService', ['getGradeScales', 'getGradeScale']);
  const mockGrades = grades;
  const mockCourseGrades = courseGrades;
  const mockCourseGrades1 = courseGrades1;
  // Because we are testing the actual service here,
  // we will not be providing a spy object for the service
  // Instead, we are going to give to each unit test a separate
  // instance of the service.
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MostModule.forRoot({
          base: '/',
          options: {
            useMediaTypeExtensions: false
          }
        }),
        ApiTestingModule.forRoot()
      ],
      providers: [
        {
          provide: GradeScaleService,
          useValue: gradeScaleSvc
        },
        GradesService
      ],
      declarations: []
    }).compileComponents();
    mockApi = TestBed.get(ApiTestingController);
  }));
  it('should fetch all grades', inject([GradesService], (gradesService: GradesService) => {
    mockApi.match({
        url: '/students/me/grades/',
        method: 'GET'
      }
    ).map((request: TestRequest) => {
      request.flush({value: []});
    });
    gradesService.getAllGrades().then(res => {
      expect(res).toBeTruthy();
      expect(res.value.length).toEqual(0);
    });
  }));
  it('should fetch grade info', inject([GradesService], (gradesService: GradesService) => {
    mockApi.match({
      url: '/students/me/courses/',
      method: 'GET'
    }).map(request => {
      request.flush({
        value: []
      });
    });
    gradesService.getGradeInfo().then(res => {
      expect(res).toBeTruthy();
      expect(res.value.length).toEqual(0);
    });
  }));
  it('should fetch theses', inject([GradesService], (gradesService: GradesService) => {
    mockApi.match({
      url: '/students/me/theses/',
      method: 'GET'
    }).map((request: TestRequest) => {
      request.flush({
        value: []
      });
    });
    gradesService.getThesisInfo().then(res => {
      expect(res).toBeTruthy();
      expect(res.value.length).toEqual(0);
    });
  }));
  it('should fetch recent grades', inject([GradesService], (gradesService: GradesService) => {
    mockApi.match({
      url: '/students/me/grades/',
      method: 'GET'
    }).map((request: TestRequest) => {
      request.flush({
        value: []
      });
    });
    gradesService.getRecentGrades().then(res => {
      expect(res).toBeTruthy();
      expect(res.value.length).toEqual(0);
    });
  }));
  it('should throw error when calculating the grades simple average', inject([GradesService], (gradesService: GradesService) => {
    mockApi.match({
      url: '/students/me/grades/',
      method: 'GET'
    }).map((request: TestRequest) => {
      request.flush({
        value: []
      });
    });
    gradesService.getRecentGrades().then(res => {
      expect(gradesService.getGradesSimpleAverage(res)).toThrowError('Courses must be an Array');
    });
  }));
  it('should return the simple average of the grades', inject([GradesService], (gradesService: GradesService) => {
    mockApi.match({
      url: '/students/me/grades/',
      method: 'GET'
    }).map((request: TestRequest) => {
      request.flush(mockGrades);
    });
    gradesService.getRecentGrades().then(res => {
      const average = gradesService.getGradesSimpleAverage(res);
      expect(average).toBeTruthy();
      expect(average.average).toEqual(7.45);
      expect(average.courses).toEqual(5);
      expect(average.passed).toBe(2);
    });
  }));
  it('should return 0 because the coefficient sum is zero', inject([GradesService], (gradesService: GradesService) => {
    mockApi.match({
      url: '/students/me/grades/',
      method: 'GET'
    }).map((request: TestRequest) => {
      request.flush(mockGrades);
    });
    // This should return zero because the coefficient is zero
    gradesService.getRecentGrades().then(res => {
      expect(gradesService.getGradesWeightedAverage(res)).toBeDefined();
    });
  }));
  // we are passing a course array with non zero length and where at least one of the courses has a non-zero coefficient
  it('should return the weighted average of courses', inject([GradesService], (gradesService: GradesService) => {
    mockApi.match({
      url: '/students/me/courses/',
      method: 'GET'
    }).map((request: TestRequest) => {
      request.flush(mockCourseGrades);
    });
    gradesService.getGradeInfo().then(res => {
      const average: GradeAverageResult = gradesService.getGradesWeightedAverage(res.value);
      expect(average).toBeDefined();
      expect(average.grades).toEqual(0.61);
      expect(average.passed).toEqual(1);
      expect(average.courses).toEqual(10);
      expect(average.coefficients).toEqual(1);
      expect(average.average).toEqual(0.61);
      expect(average.ects).toEqual(6);
      expect(average.units).toEqual(0);
    }).catch(err => {
      Promise.reject(err);
      fail();
    });
  }));
  // we are passing a course array of zero length
  it('should return an average of 0' , inject([GradesService], (gradesService: GradesService) => {
    mockApi.match({
      url: '/students/me/courses/',
      method: 'GET'
    }).map((request: TestRequest) => {
      request.flush({
        value: []
      });
    });
    gradesService.getGradeInfo().then(res => {
      const average = gradesService.getGradesWeightedAverage(res.value);
      expect(average).toBeDefined();
      expect(average.grades).toEqual(0.61);
    });
  }));
  // we are passing a course array of non-zero length and with complex courses, courses whose grade doesn't count to
  // the grade or whose units don't count to getting the degree.
  it('should return the average of the courses' , inject([GradesService], (gradesService: GradesService) => {
    mockApi.match({
      url: '/students/me/courses/',
      method: 'GET'
    }).map((request: TestRequest) => {
      request.flush(mockCourseGrades1);
    });
    gradesService.getGradeInfo().then(res => {
      const average = gradesService.getGradesWeightedAverage(res.value);
      expect(average).toBeDefined();
      expect(average.grades).toEqual(4.7);
      expect(average.passed).toEqual(3);
      expect(average.courses).toEqual(8);
      expect(average.coefficients).toEqual(6.5);
      expect(average.average).toEqual(0.7231);
      expect(average.ects).toEqual(21);
      expect(average.units).toEqual(23);
    });
  }));

  afterEach(() => {
    sessionStorage.clear();
  });
});

